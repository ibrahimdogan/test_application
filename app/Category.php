<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable=['category_name','created_at','updated_at'];
    protected $table='category';

//   kategori ve postlar için pivot tablo oluşturuldu
    public function post()
    {
        return $this->belongsToMany('App\Post');
    }
// kategorinin altında bulunun postlar özerinde işlem yapmak için bağlantı yapıldı
    public function posts_under_category()
    {
        return $this->hasMany('App\CategoryPost');
    }




   


}
