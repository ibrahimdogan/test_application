<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable=['author_id','post_name','content','image_url','created_at','updated_at'];
    protected $table='post';

  // kategori ve postlar için pivot tablo oluşturuldu
  public function category()
  {
      return $this->belongsToMany('App\Category');
  }
 // Post kapsayan categoriler özerinde işlem yapmak için ilişki kuruldu
  public function category_under_post()
  {
      return $this->hasMany('App\CategoryPost');
  }
// Post un hangi yazara ait olduğunu anlamak için bu ilişki kuruldu
  public function authors()
  {
      return $this->hasMany('App\Post','author_id');
  }

  public function getcontentAttribute($value)
  {
      return ucwords($value);
  }

  public function getpostnameAttribute($value)
  {
    return ucwords($value);
  }

}
