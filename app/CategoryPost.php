<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryPost extends Model
{
    protected $fillable=['category_id','post_id','updated_at','created_at'];
    protected $table='category_post';
    
}
