<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Author extends Authenticatable
{
    use Notifiable;

    protected $fillable=[
        'users_id','author_name','author_email','author_sifre','created_at','updated_at'
    ];
    public $table='author';
    

    protected $hidden = [
        'password', 'remember_token',
    ];
  // Yazar ile admin arasındaki ilişki kuruldu
    public function users()
    {
        return $this->hasMany('App\Author','users_id');
    }

    public function getauthornameAttribute($value)
    {
        return ucwords($value);
    }
   
    public function getauthorpasswordAttribute($value)
    {
        return "............";
    }
}
