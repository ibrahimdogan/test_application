<?php

namespace App\Http\Controllers;
use App\Post;
use App\Category;

use DB;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index()
    {
        $categoryList=DB::table('category')->get();
        return view('customer.index',['categoryList'=>$categoryList]);
    }

    public function category_detail($id)
    { 
       $categoryDetail=Category::find($id)->post;
        return view('customer.postList',['categoryDetail'=>$categoryDetail]);
    }

    public function post_detail($id)
    { 
        $post=Post::find($id);
        return view('customer.postDetailList',['post'=>$post]);
    }
}
