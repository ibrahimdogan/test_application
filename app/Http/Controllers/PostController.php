<?php

namespace App\Http\Controllers;

use App\Post;
use App\Category;
use App\CategoryPost;
use DB;
use Illuminate\Http\Request;

class PostController extends Controller
{
  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!(session()->get('author_Id')))
        {
            return view('author.authorLogin');
        }
        $postList=Post::find(session()->get('author_Id'))->authors;
       return view('post.listPost',['postList'=>$postList]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoryList=DB::table('category')->get();
        return view('post.newPost',['categoryList'=>$categoryList]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {       
        //Resim taşıma işlemi 
        $imageName = time().'.'.request()->image_url->getClientOriginalExtension();
        request()->image_url->move(public_path('posts_image'), $imageName);
        //yeni post kayıt etme
        $newPost=new Post();
        $newPost->post_name=$request->post_name;
        $newPost->author_id=1;
        $newPost->image_url=$imageName;
        $newPost->content=$request->content;
        $newPost->save();
        //category_post tablosuna kayıt işlemi
        $category = Category::find($request->category_name);
        $newPost->category()->attach($category);
        return redirect('post');

    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editPostList=Post::find($id);
        $linkedCategories=Post::find($id)->category;
        return view('post.newPost',['editPostList'=>$editPostList,'linkedCategories'=>$linkedCategories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $imageName = time().'.'.request()->image_url->getClientOriginalExtension();
        request()->image_url->move(public_path('posts_image'), $imageName);
        $updatePost=Post::find($id);
        $updatePost->post_name=$request->post_name;
        $updatePost->author_id=1;
        $updatePost->image_url=$imageName;
        $updatePost->content=$request->content;
        $updatePost->save();
        return redirect('post');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //tek sorguda iki tablodanda silme işlemi yapılacak
        $deletePost=Post::find($id);
        $deleteCategoryUnderPost=Post::find($id)->posts_under_category;
        $deleteCategoryUnderPost->each->delete();
        $deletePost->delete();
        return('post');
    }

    // ilerde auth işlemi ile yapılacak
    public function authorLogin(Request $request)
    {
       $kisiKayitlimi=DB::table('author')->where('author_email',$request->author_email)->get();
       if(count($kisiKayitlimi)!=0){
         session()->put('author_Id',$kisiKayitlimi[0]->id);
         return redirect('post');
       } 
       else
       {
        return view('author.authorLogin');
       }
    }

    public function sessionDelete()
    {
        session()->forget('author_Id');
        return view('author.authorLogin');
    }
}
