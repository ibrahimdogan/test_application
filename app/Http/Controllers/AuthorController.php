<?php

namespace App\Http\Controllers;
use App\User;
use App\Author;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $authorList=Author::find(Auth::id())->users;
        return View('author.listAuthor',['authorList'=>$authorList]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('author.newAuthor');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newAuthor=new Author();
        $newAuthor->users_id=Auth::id();
        $newAuthor->author_name=$request->author_name;
        $newAuthor->author_email=$request->author_email;
        $newAuthor->author_password=$request->author_password;
        $newAuthor->save();
        return redirect('author');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function show(Author $author)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $authorList=Author::find($id);
        return view('author.newAuthor',['authorList'=>$authorList]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $updateAuthor=Author::find($id);
        $updateAuthor->author_name=$request->author_name;
        $updateAuthor->author_email=$request->author_email;
        $updateAuthor->author_password=$request->author_password;
        $updateAuthor->save();
        return redirect('author');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteAuthor=Author::find($id);
        $deleteAuthor->delete();
        return redirect('author');
    }

  
}
