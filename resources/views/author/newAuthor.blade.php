<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>

<form class="form-horizontal" method="post" action="/author{{isset($authorList) ? '/'.$authorList->id:''}}">
{{csrf_field()}}

@if(isset($authorList))
    <input type="hidden" name="_method" value="PUT">
@endif
<!-- Form Name -->
<legend>New Author</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="author_name">Author Name</label>  
  <div class="col-md-4">
  <input id="author_name" name="author_name" type="text" placeholder="enter author name..." value="{{isset($authorList) ? $authorList->author_name:''}}"  class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="author_email">Author Email</label>  
  <div class="col-md-4">
  <input id="author_email" name="author_email" type="text" placeholder="enter author email" value="{{isset($authorList) ? $authorList->author_email:''}}" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">author_password</label>  
  <div class="col-md-4">
  <input id="textinput" name="author_password" type="password" placeholder="enter author password" value="{{isset($authorList) ? $authorList->author_password:''}}" class="form-control input-md">
    
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="singlebutton"></label>
  <div class="col-md-4">
    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Add</button>
  </div>
</div>

</fieldset>
</form>
