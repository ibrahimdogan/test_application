<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
<form class="form-horizontal" method="post" action="/post{{isset($editPostList) ? '/'.$editPostList->id:''}}" enctype="multipart/form-data">
{{csrf_field()}}

@if(isset($editPostList))
    <input type="hidden" name="_method" value="PUT">
@endif
<fieldset>

<!-- Form Name -->
<legend>new Post</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="post_name">Post Title</label>  
  <div class="col-md-4">
  <input id="post_name" name="post_name" type="text" placeholder="enter post title" value="{{isset($editPostList) ? $editPostList->post_name:''}}" class="form-control input-md">
    
  </div>
</div>
@if(isset($linkedCategories))
<!-- Select Multiple -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectmultiple">Linked Categories</label>
  <div class="col-md-4">
    <select id="selectmultiple" name="category_name[]" class="form-control" multiple="multiple">
     @foreach($linkedCategories as $category)
      <option disabled value="{{$category->id}}">{{$category->category_name}}</option>
      @endforeach
    </select>
  </div>
</div>
@else
<!-- Select Multiple -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectmultiple">Select Category</label>
  <div class="col-md-4">
    <select id="selectmultiple" name="category_name[]" class="form-control" multiple="multiple">
      
     @foreach($categoryList as $category)
      <option value="{{$category->id}}">{{$category->category_name}}</option>
      @endforeach
    </select>
  </div>
</div>
@endif
<!-- File Button --> 
<div class="form-group">
  <label class="col-md-4 control-label" for="filebutton">Select Image</label>
  <div class="col-md-4">
    <input id="filebutton" name="image_url" class="input-file" value="{{isset($editPostList) ? $editPostList->image_url:''}}" type="file">
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="content">Content</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="content" name="content">{{isset($editPostList) ? $editPostList->content:''}}</textarea>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="Add"></label>
  <div class="col-md-4">
    <button id="Add" name="Add" class="btn btn-primary">Add</button>
  </div>
</div>

</fieldset>
</form>
