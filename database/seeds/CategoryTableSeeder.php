<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category=['java','php','go','python','c','ruby'];
        for($i=0;$i<6;$i++)
        {
            DB::table('category')->insert([
                'category_name' =>$category[$i],
            ]);
        }
    }
}
