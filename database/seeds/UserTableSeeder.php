<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    for($i=1;$i<5;$i++)
        {
            DB::table('users')->insert([
                'name' => "user_".$i,
                'email' => 'user_'.$i.'@gmail.com',
                'password' => bcrypt('user_'.$i),
            ]);
        }
    }
}
