<?php

use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1;$i<10;$i++)
        {
            DB::table('post')->insert([
                'author_id' => $i%3+1,
                'post_name' =>"post_".$i,
                'content' => "post content _".$i,
                'image_url' => "1560757564.jpg",
            ]);
        }
    }
}
