<?php

use Illuminate\Database\Seeder;

class AuthorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1;$i<10;$i++)
        {
            DB::table('author')->insert([
                'users_id' =>1+$i%4,
                'author_name' => 'author_'.$i,
                'author_email' => 'author_'.$i.'@gmail.com',
                'author_password' => bcrypt('author_'),
            ]);
        }
       
    }
}
