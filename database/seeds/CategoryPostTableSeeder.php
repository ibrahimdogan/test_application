<?php

use Illuminate\Database\Seeder;

class CategoryPostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categori_id=['1','6','2','4','1','5','3','2','3','4','1','5','6','4','5'];
        $post_id=['1','2','1','2','4','3','2','1','4','2','5','6','7','8','9'];
        for($i=0;$i<15;$i++)
        {
            DB::table('category_post')->insert(
                [
                'category_id' =>$categori_id[$i],
                'post_id' =>$post_id[$i],
                ]);
        }
    }
}
