<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (){
    return view('header');
});

Route::resource('author','AuthorController');
Route::resource('category','CategoryController');
Route::resource('post','PostController');
Route::resource('customer','CustomerController');
Route::get('category_detail/{id}','CustomerController@category_detail');
Route::get('post_detail/{id}','CustomerController@post_detail');

Route::post('authorlogin','PostController@authorLogin');
Route::get('sessiondelete','PostController@sessionDelete');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');